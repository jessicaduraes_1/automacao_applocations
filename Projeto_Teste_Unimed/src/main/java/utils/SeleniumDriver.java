package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class SeleniumDriver {

    private static SeleniumDriver seleniumDriver;
    private static WebDriver driver;

    public final static int TIMEOUT = 30;
    public final static int PAGE_LOAD_TIMEOUT = 20;

    private SeleniumDriver() {

        String browserType = System.getProperty("Chrome");
        if(browserType != null)
            System.out.println(browserType);
        
        	System.setProperty("webdriver.chrome.driver", "C:/Users/Jessica/Desktop/DESAFIO/DESAFIO1/chromedriver.exe");

        	driver = new ChromeDriver();
        	driver.manage().window().maximize();
        
    }

    public static void openPage(String url) {
       driver.navigate().to(url);
    
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setUpDriver() {
        if (seleniumDriver == null)
            seleniumDriver = new SeleniumDriver();
    }

    public static void tearDown() {
        if (driver != null) {
            driver.close();
            driver.quit();
        }
        seleniumDriver = null;
    }
}
