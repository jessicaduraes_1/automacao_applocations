package utils;


import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class SeleniumHelper {

    public static boolean isElementPresent(WebElement webElement) {
        try {
            boolean isPresent = webElement.isDisplayed();
            return isPresent;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
    
    public static void isElementValueValid(WebElement webElement, String texto) {
    	boolean isDisplayed = webElement.isDisplayed();
    	if (isDisplayed){
    		String valor = webElement.getAttribute("value");
    		Assert.assertTrue(valor.equals(texto));
    	}
    }
    
    public static void isElementValueNotDisplayed(WebElement webElement, String texto) {
    	String valor = webElement.getAttribute("value");
    	if (valor != texto ){
    		Assert.assertTrue(true);
    		
    	}
    }
    
    
    
}
