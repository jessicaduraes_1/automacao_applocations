package pages.actions;

import org.openqa.selenium.support.PageFactory;
import pages.locators.PaginaPesquisaLocators;
import utils.SeleniumDriver;
import utils.SeleniumHelper;


public class PaginaPesquisaSiteUnimedActions {

    private PaginaPesquisaLocators locators;
    
    public PaginaPesquisaSiteUnimedActions() {
        locators = new PaginaPesquisaLocators();
        PageFactory.initElements(SeleniumDriver.getDriver(), locators);
    }
    

    public void abrirPagina() {
    		SeleniumDriver.openPage("https://www.unimed.coop.br/site/");
    }
    
    public void acessarGuiaMedico() {
    	SeleniumHelper.isElementPresent(locators.OpçãoConsultaGuiaMédico);
        locators.OpçãoConsultaGuiaMédico.click();
    }
    
    public void acionarBuscaRápida() {
    	SeleniumHelper.isElementPresent(locators.OpçãoBuscaRápida);
        locators.OpçãoBuscaRápida.click();
    }
    
    public void pesquisar(String texto) {
      	SeleniumHelper.isElementPresent(locators.ElementoPesquisa);
        locators.ElementoPesquisa.sendKeys(texto);
    }
    
    public void acionarPesquisar() {
    	SeleniumHelper.isElementPresent(locators.OpçãoPesquisar);		
        locators.OpçãoPesquisar.click();
    }
        
}
