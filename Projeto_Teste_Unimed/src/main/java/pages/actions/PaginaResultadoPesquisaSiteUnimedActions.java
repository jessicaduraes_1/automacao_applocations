package pages.actions;

import org.openqa.selenium.support.PageFactory;
import pages.locators.PaginaResultadoPesquisaLocators;
import utils.SeleniumDriver;
import utils.SeleniumHelper;


public class PaginaResultadoPesquisaSiteUnimedActions {

    private PaginaResultadoPesquisaLocators locators;
    
    public PaginaResultadoPesquisaSiteUnimedActions() {
        locators = new PaginaResultadoPesquisaLocators();
        PageFactory.initElements(SeleniumDriver.getDriver(), locators);
    }
    
 
    public void validarValorElementoPesquisado(String texto) { 	
    	SeleniumHelper.isElementValueValid(locators.ElementoPesquisadoResultado, texto);
    	
    }
    
    public void validarValorElementoPesquisadoEspecialidade(String texto) { 	
    	SeleniumHelper.isElementValueValid(locators.ItemElementoPesquisadoResultadoEspecialidade, texto);
    	
    }
    
    public void validarValorElementoPesquisadoCidade(String texto) { 	
    	SeleniumHelper.isElementValueValid(locators.ItemElementoPesquisadoResultadoCidade, texto);
    	
    }
    
    public void validarValorElementoNãoPesquisado(String texto) { 	
    	SeleniumHelper.isElementValueNotDisplayed(locators.ElementoPesquisadoResultado, texto);
    }

    
}
