package pages.locators;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PaginaPesquisaLocators {

    @FindBy(how = How.ID, using = "fragment-oimm-link")
    public WebElement OpçãoConsultaGuiaMédico;
    
    @FindBy(how = How.XPATH, using = "//div[@id='search-query-input']/div/div")
    public WebElement OpçãoBuscaRápida;
    
    @FindBy(how = How.ID, using = "react-select-2-input")
    public WebElement ElementoPesquisa;
    
    @FindBy(how = How.XPATH, using = "//div[@id='react-tabs-1']/form/div/div[3]/div/button")
    public WebElement OpçãoPesquisar;
    
}

