package pages.locators;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PaginaResultadoPesquisaLocators {
    
    @FindBy(how = How.ID, using = "react-select-3-input")
    public WebElement ElementoPesquisadoResultado;
    
    @FindBy(how = How.XPATH, using = "//div[@id='gm-v3-root']/div/div[2]/div[2]/div/div[2]/div/div/div/div[3]/span")
    public WebElement ItemElementoPesquisadoResultado;
    
    @FindBy(how = How.XPATH, using = "//*[contains(text(), 'Psicologia')]")
    public WebElement ItemElementoPesquisadoResultadoEspecialidade;
    
    @FindBy(how = How.XPATH, using = "//*[contains(text(), 'Rio de Janeiro')]")
    public WebElement ItemElementoPesquisadoResultadoCidade;  

    
    
    
}

