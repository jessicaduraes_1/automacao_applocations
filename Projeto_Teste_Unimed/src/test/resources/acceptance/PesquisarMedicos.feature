@acceptance

  Feature: Pesquisar por medicos no site da Unimed
 
  Background:  Para pesquisar por medicos no site da Unimed
  o paciente devera acessar o Guia Medico do site 

  Scenario: Pesquisar por medicos no Rio de Janeiro
  
   Given Que Eu estou na pagina inicial da Unimed
   When Eu acesso o  Guia Medico
   Then Eu devo pesquisar por medicos no Rio de Janeiro
    
   Scenario: Pesquisar por Especialidade medica
  
   Given Que Eu estou na pagina inicial da Unimed
   When Eu acesso o  Guia Medico
   Then Eu devo pesquisar por Especialidade medica
   