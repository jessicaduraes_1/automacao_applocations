package hooks;


import cucumber.api.java.After;
import utils.SeleniumDriver;

public class AfterActions {

	
    @After
    public void tearDown() {
    	 try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
        SeleniumDriver.tearDown();
    }
    
    
}
