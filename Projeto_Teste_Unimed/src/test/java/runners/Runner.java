package runners;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"json:target/cucumber.json", "pretty", "html:target/cucumber.html"},
        features = "src/test/resources/acceptance",
        glue = "src/test/java/steps",
        tags = {"@acceptance"},
        dryRun = false,
        strict = true)

public class Runner {
}


