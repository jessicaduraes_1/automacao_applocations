package steps;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;


import pages.actions.PaginaPesquisaSiteUnimedActions;
import pages.actions.PaginaResultadoPesquisaSiteUnimedActions;

public class PesquisarMedicosSteps {

    private PaginaPesquisaSiteUnimedActions paginaPesquisaSiteUnimedActions;
    private PaginaResultadoPesquisaSiteUnimedActions paginaResultadoPesquisaSiteUnimedActions;

    public PesquisarMedicosSteps() {
        this.paginaPesquisaSiteUnimedActions = new PaginaPesquisaSiteUnimedActions();
        this.paginaResultadoPesquisaSiteUnimedActions = new PaginaResultadoPesquisaSiteUnimedActions();  
 
    }

    @Dado("^Que Eu estou na pagina inicial da Unimed$")
    public void Eu_estou_na_página_inicial_da_Unimed() throws Throwable {
        paginaPesquisaSiteUnimedActions.abrirPagina();
   
    }
    
    @Quando("^Eu acesso o  Guia Medico$")
    public void Eu_acesso_o_Guia_Medico() throws Throwable {
        paginaPesquisaSiteUnimedActions.acessarGuiaMedico();
    }
    
  
    @Então("^Eu devo pesquisar por medicos no Rio de Janeiro")
    public void Eu_devo_pesquisar_por_medicos_no_Rio_de_Janeiro() throws Throwable {
    	paginaPesquisaSiteUnimedActions.acionarBuscaRápida();
    	paginaPesquisaSiteUnimedActions.pesquisar("Rio de Janeiro");
    	paginaPesquisaSiteUnimedActions.acionarPesquisar();
    	paginaResultadoPesquisaSiteUnimedActions.validarValorElementoPesquisado("Rio de Janeiro");
    	paginaResultadoPesquisaSiteUnimedActions.validarValorElementoPesquisadoCidade("Rio de Janeiro");
    	paginaResultadoPesquisaSiteUnimedActions.validarValorElementoNãoPesquisado("São Paulo");
    	 	
    }
    @Então("^Eu devo pesquisar por Especialidade medica")
    public void Eu_devo_pesquisar_Especialidade_medica() throws Throwable {
    	
    	paginaPesquisaSiteUnimedActions.pesquisar("Psicologia");
    	paginaPesquisaSiteUnimedActions.acionarPesquisar();
    	paginaResultadoPesquisaSiteUnimedActions.validarValorElementoPesquisado("Psicologia");
    	paginaResultadoPesquisaSiteUnimedActions.validarValorElementoPesquisadoEspecialidade("Psicologia");	
    	paginaResultadoPesquisaSiteUnimedActions.validarValorElementoNãoPesquisado("São Paulo");
    }
    
}
