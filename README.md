Desafio_1: Projeto_Teste_Unimed

Pré-requisitos: Kit de Desenvolvimento Java (JDK), Java Eclipse IDE configurados e navegador Chrome.
Java Eclipse IDE, acionar: File -> open project from file system. Acionar opção "Archive". Selecionar o arquivo.zip do projeto. Acionar "Finish".
No project explorer, Acionar com o botão direito do mouse -> Build path -> Configure build path. Acionar "Add External JARs". Selecionar arquivos contidos 
na pasta "bibliotecas de projeto", incluindo a pasta "lib" (em https://www.dropbox.com/sh/true5q6brbqgzst/AACNDJ4iZ4FUvF_Kpzl6xXh-a?dl=0). Aplicar.
Acessar (https://chromedriver.chromium.org/downloads) , baixar, configurar a variável de ambiente do sistema e extrair o driver conforme versão do navegador Chrome intalada na máquina.
Na estrutura do projeto, em src/main.java.utils, acessar o SeleniumDriver.java e ajustar a linha
System.setProperty("webdriver.chrome.driver", "C:/Users/Jessica/Desktop/DESAFIO/DESAFIO1/chromedriver.exe");
para 
System.setProperty("webdriver.chrome.driver", "pathdochromedriver/chromedriver.exe");
Dessa forma, o caminho onde o driver foi baixado no computador e versões para execução do driver estarão corretos.
Versão atual do chromedriver: 97.
Run as Cucumber Feature o arquivo "PesquisarMedicos.feature"
(estrutura de pastas src/test/resources/acceptance do projeto)

Desafio_2: Projeto_OMDB_API                   

Para rodar o Desafio 2, ter o POSTMAN instalado (https://www.postman.com/downloads/).
Em seguida, acionar a opção "File" -> "Import" -> "Upload files".
Selecionar "OMDB_API.postman_collection.json" e acionar a opção "Import".
Para rodar os testes: Na collection OMDB_API, acessar a request "Busca Filme" e acionar a aba "Tests". Em seguida, acionar a opção "Send".
Visualizar resultado do teste na aba "Test Results".
Realizar o mesmo procedimento para a request "Busca Filme Inexistente".

Desafio_3: Projeto_Logista

Para rodar o Desafio 3, ter o Node.js instalado (https://nodejs.org/en/download/).
Em seguida, acessar a pasta do projeto, e no terminal, executar "npm install" 
                                                                "npm install --save-dev jest@27.4.7
                                                                "npm install --save-dev standard@16.0.4"
                                                                "npm run test -- --watchAll"
                                                                
Desafio_4: AppiumAutomacao (Iniciado mas não finalizado)

Instalação:
ferramenta AppiumAndroid studio

(https://developer.android.com/studio/index.html?hl=pt-br)

Java
Para baixar o jdk8 compatível com o sistema operacional usado.

Após a instalação é necessário setar as variáveis de ambiente JAVA_HOME e ANDROID_HOME

Instalar o Appium
https://appium.io/downloads.html
                                                                
