Para rodar o Desafio 2, ter o POSTMAN instalado (https://www.postman.com/downloads/).
Em seguida, acionar a opção "File" -> "Import" -> "Upload files".
Selecionar "OMDB_API.postman_collection.json" e acionar a opção "Import".
Para rodar os testes: Na collection OMDB_API, acessar a request "Busca Filme" e acionar a aba "Tests". Em seguida, acionar a opção "Send".
Visualizar resultado do teste na aba "Test Results".
Realizar o mesmo procedimento para a request "Busca Filme Inexistente".