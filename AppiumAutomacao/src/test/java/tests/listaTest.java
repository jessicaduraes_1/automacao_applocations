package tests;

import java.net.MalformedURLException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.MobileBy;

public class listaTest {
	
    baseClass driverFactory = new baseClass();
    @BeforeTest
    
    public void inicializarDriver() throws MalformedURLException {
        driverFactory = new baseClass();
//        driverFactory.setCapabilities("Android", "Chrome", "", "",
//                "");
        driverFactory.setup();

    }

    @AfterTest
    public void finalizarDriver(){
        driverFactory.teardown();
    }

    @Test
    public void acessarMenu()  {

       
        clickid("opcaomenucapturadoAppium");
        clickid("opcaolistacapturadoAppium");
        clickid("idDigiteUmTermocapturadoAppium");
        sendkeys("idDigiteUmTermocapturadoAppium","Lo");
       
        String resultado = driverFactory.findElement(MobileBy.partialLinkText("Melo"));
        
        boolean isDisplayed = resultado.isDisplayed();
    	if (isDisplayed){
    	
    		Assert.assertTrue();
    	} 
    }	
    

    public void clickid(String id){
        driverFactory.findElement(MobileBy.id(id)).click();

    }
    
    public void sendkeys(String id, String texto){
        driverFactory.findElement(MobileBy.id(id)).sendkeys(texto);

    }

 }
	
		
	
