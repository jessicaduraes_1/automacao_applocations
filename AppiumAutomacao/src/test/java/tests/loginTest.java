package tests;
import io.appium.java_client.MobileBy;
import java.net.MalformedURLException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class loginTest {


	    baseClass driverFactory = new baseClass();
	    @BeforeTest
	    
	    public void inicializarDriver() throws MalformedURLException {
	        driverFactory = new baseClass();
	//        driverFactory.setCapabilities("Android", "Chrome", "", "",
//	                "");
	        driverFactory.setup();

	    }

	    @AfterTest
	    public void finalizarDriver(){
	        driverFactory.teardown();
	    }

	    @Test
	    public void efetuarLoginComSucesso()  {

	        sendkeys("idlogincapturadoAppium","admin");
	        sendkeys("idsenhacapturadoAppium","admin");
	        clickid("opcaoLogicapturadoAppium");
	       
	        String resultado = driverFactory.findElement(MobileBy.id("idelementopaginainicial");
	        
	        boolean isDisplayed = resultado.isDisplayed();
	    	if (isDisplayed){
	    		String valor = resultado.getText();
	    		Assert.assertTrue(valor.equals("Bem vindo ao seu desafio!"));
	    	} 
	    }	
	    	
	    @Test
	  	public void efetuarLoginDadosIncorretos()  {

	  	  sendkeys("idlogincapturadoAppium","sdfsg");
	  	  sendkeys("idsenhacapturadoAppium","dfudsf");
	  	  clickid("opcaoLogicapturadoAppium");
	  	       
	  	 String mensagemLoginIncorreto = driverFactory.findElement(MobileBy.id("idelementologinincorreto");
	  	        
	  	  boolean isDisplayed = resultado.isDisplayed();
	  	  if (isDisplayed){
	  	    String valor = mensagemLoginIncorreto.getText();
	  	    Assert.assertTrue(valor.equals("Usuario e/ou senha incorreto"));
	  	    } 
	    		    	
	    	
	    }
	    
	    @Test
	  	public void efetuarLoginDadosSemPreenchimento()  {

	  	  sendkeys("idlogincapturadoAppium","");
	  	  sendkeys("idsenhacapturadoAppium","");
	  	  clickid("opcaoLogicapturadoAppium");
	  	       
	  	 String mensagemLoginIncorreto = driverFactory.findElement(MobileBy.id("idelementologinincorreto");
	  	        
	  	  boolean isDisplayed = resultado.isDisplayed();
	  	  if (isDisplayed){
	  	    String valor = mensagemLoginIncorreto.getText();
	  	    Assert.assertTrue(valor.equals("Usuario e/ou senha incorreto"));
	  	    } 
	    		    	
	    	
	    }


	    
	    }

	    public void clickid(String id){
	        driverFactory.findElement(MobileBy.id(id)).click();

	    }
	    
	    public void sendkeys(String id, String texto){
	        driverFactory.findElement(MobileBy.id(id)).sendkeys(texto);

	    }

	}
	
	
